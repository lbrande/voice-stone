﻿using System;
using System.Net.Sockets;

namespace Client
{
    internal class VoiceStoneClient
    {
        private const byte READ = 0;

        static void Main(string[] args)
        {
            var client = new TcpClient("127.0.0.1", 30310);
            var stream = client.GetStream();
            stream.WriteByte(READ);
            Console.WriteLine(stream.ReadByte());
            Console.WriteLine(stream.ReadByte());
            Console.WriteLine(stream.ReadByte());
        }
    }
}
