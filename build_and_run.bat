@echo off

TASKKILL /IM Serenade.exe /F
TASKKILL /IM HearthstoneDeckTracker.exe /F
TASKKILL /IM VoiceStoneServer.exe /F
MSBUILD -V:M %~dp0VoiceStone.sln /p:Configuration=Release
DEVENV %~dp0VoiceStone.sln /BUILD Release /PROJECT VoiceStoneInstaller
XCOPY /YF %~dp0VoiceStoneClient\bin\Release\VoiceStoneClient.exe "C:\Program Files\VoiceStone\"
XCOPY /YF %~dp0VoiceStoneServer\bin\Release\VoiceStoneServer.exe "C:\Program Files\VoiceStone\"
XCOPY /YF %~dp0VoiceStonePlugin\bin\Release\VoiceStonePlugin.dll %APPDATA%\HearthstoneDeckTracker\Plugins\
XCOPY /YFS %~dp0VoiceStoneCommands %HOMEDRIVE%%HOMEPATH%\.serenade\scripts\voice_stone\
START /B %LOCALAPPDATA%\HearthstoneDeckTracker\HearthstoneDeckTracker.exe
START /B %LOCALAPPDATA%\Programs\Serenade\Serenade.exe
