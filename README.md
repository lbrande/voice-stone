# Voice Stone

Voice Stone is a tool that enables you to play Hearthstone using only your voice.

Currently only standard Hearthstone is supported. In the future I hope to support other game modes as well as interacting with the menus.

## Requirements

* [Hearthstone Deck Tracker](https://hsreplay.net/downloads/) (for extracting necessary game data)
* [Serenade](https://serenade.ai/) (for detecting voice commands)
