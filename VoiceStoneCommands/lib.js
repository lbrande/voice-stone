const screenProfile = require("./screen_profile_2560_1440.js");

const NUMBERS = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
];

var api;

function numberRange(end) {
    return [...NUMBERS].slice(0, end);
}

function locationRange(start, end) {
    let steps = Math.floor(Math.abs(end.x - start.x) / start.step) + 1;
    return [...Array(steps)].map((_, index) => {
        return {
            x: start.x + start.step * index * (end.x < start.x ? -1 : 1),
            y: start.y
        };
    });
}

function defineCommand(trigger, callback) {
    serenade.app("Hearthstone.exe").command(
        trigger,
        async _api => {
            api = _api;
            await callback(await getGameData());
            return hoverLocation(screenProfile.HIDE_SHOW_BUTTON);
        });
}

function defineHoverCommand(trigger, callback) {
    serenade.app("Hearthstone.exe").command(
        trigger,
        async _api => {
            api = _api;
            return callback(await getGameData());
        });
}

async function getGameData() {
    let result = await api.runShell("VoiceStoneClient.exe", {
        shell: true,
        cwd: "C:/Program Files/VoiceStone",
    });
    result = result.stdout.split("\n");
    return {
        handSize: result[0],
        boardSize: result[1],
        enemyBoardSize: result[2],
    }
}

function getLocation(location, index) {
    if (index === undefined) {
        return location;
    }
    return {
        x: location.x + location.step * index,
        y: location.y,
    };
}

async function hoverLocation(location, index) {
    location = getLocation(location, index);
    await api.setMouseLocation(location.x, location.y);
    return new Promise(resolve => setTimeout(resolve, 100));
}

async function clickLocation(location, index, button) {
    await hoverLocation(location, index);
    if (button === undefined) {
        button = "left";
    }
    await api.click(button);
    return new Promise(resolve => setTimeout(resolve, 100));
}

module.exports = {
    numberRange: numberRange,
    locationRange: locationRange,
    defineCommand: defineCommand,
    defineHoverCommand: defineHoverCommand,
    hoverLocation: hoverLocation,
    clickLocation: clickLocation,
}
