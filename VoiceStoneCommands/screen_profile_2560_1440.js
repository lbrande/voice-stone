const CENTER_Y = 672;
const BOTTOM_Y = 1440;

module.exports = {
    MULLIGAN_3: { x: 828, y: CENTER_Y, step: 457 },
    MULLIGAN_4: { x: 775, y: CENTER_Y, step: 342 },
    CONFIRM_BUTTON: { x: 1285, y: 1144 },

    HAND: [
        { x: 1230, y: BOTTOM_Y, step: 0 },
        { x: 1140, y: BOTTOM_Y, step: 175 },
        { x: 1050, y: BOTTOM_Y, step: 175 },
        { x: 990, y: BOTTOM_Y, step: 150 },
        { x: 955, y: BOTTOM_Y, step: 125 },
        { x: 935, y: BOTTOM_Y, step: 100 },
        { x: 915, y: BOTTOM_Y, step: 90 },
        { x: 900, y: BOTTOM_Y, step: 80 },
        { x: 890, y: BOTTOM_Y, step: 70 },
        { x: 885, y: BOTTOM_Y, step: 60 },
    ],

    HERO_POWER: { x: 1515, y: 1102 },
    TRADE: { x: 2159, y: 861 },
    
    CHOOSE_LEFT: { x: 524, y: CENTER_Y, step: 342 },
    CHOOSE_RIGHT: { x: 1948, y: CENTER_Y, step: 342 },
    CHOOSE_3: { x: 758, y: CENTER_Y, step: 518 },
    HIDE_SHOW_BUTTON: { x: 720, y: 1089 },
    
    BOARD: { x: 739, y: 787, step: 178 },
    ENEMY_BOARD: { x: 739, y: 540, step: 178 },
    HERO: { x: 1284, y: 1095 },
    ENEMY_HERO: { x: 1284, y: 256 },
    
    END_TURN_BUTTON: { x: 2082, y: CENTER_Y },
    
    SKIP: { x: 714, y: CENTER_Y },
}
