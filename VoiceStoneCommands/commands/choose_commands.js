const lib = require("../lib.js");
const screenProfile = require("../screen_profile_2560_1440.js");

lib.numberRange(4).map((position, index) =>
    lib.defineCommand(`replace ${position}`, gameData => {
        if (gameData.handSize == 3) {
            return lib.clickLocation(screenProfile.MULLIGAN_3, index);
        } else if (gameData.handSize == 5) {
            return lib.clickLocation(screenProfile.MULLIGAN_4, index);
        }
    })
);

lib.defineCommand("confirm", () => {
    return lib.clickLocation(screenProfile.CONFIRM_BUTTON);
});

lib.defineCommand("choose left", async () => {
    let locations = lib.locationRange(
        screenProfile.CHOOSE_LEFT,
        screenProfile.CHOOSE_RIGHT);
    for (let location of locations) {
        await lib.clickLocation(location);
    }
});

lib.defineCommand("choose right", async () => {
    let locations = lib.locationRange(
        screenProfile.CHOOSE_RIGHT,
        screenProfile.CHOOSE_LEFT);
    for (let location of locations) {
        await lib.clickLocation(location);
    }
});

lib.numberRange(3).map((position, index) =>
    lib.defineCommand(`choose ${position}`, () => {
        return lib.clickLocation(screenProfile.CHOOSE_3, index);
    })
);

lib.defineCommand("hide", () => {
    return lib.clickLocation(screenProfile.HIDE_SHOW_BUTTON);
});

lib.defineCommand("show", () => {
    return lib.clickLocation(screenProfile.HIDE_SHOW_BUTTON);
});
