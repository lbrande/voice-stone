const lib = require("../lib.js");
const screenProfile = require("../screen_profile_2560_1440.js");

lib.defineCommand(`hero power`, () => {
        return lib.clickLocation(screenProfile.HERO_POWER);
})

lib.numberRange(7).map((position, index) =>
    lib.defineCommand(
        `hero power target minion ${position}`,
        async gameData => {
            await lib.clickLocation(screenProfile.HERO_POWER);
            return lib.clickLocation(
                screenProfile.BOARD,
                (7 - gameData.boardSize) / 2 + index);
        })
);

lib.numberRange(7).map((position, index) =>
    lib.defineCommand(`hero power target enemy ${position}`, async gameData => {
        await lib.clickLocation(screenProfile.HERO_POWER);
        return lib.clickLocation(
            screenProfile.ENEMY_BOARD,
            (7 - gameData.enemyBoardSize) / 2 + index);
    })
)

lib.defineCommand("hero power target self", async () => {
    await lib.clickLocation(screenProfile.HERO_POWER);
    return lib.clickLocation(screenProfile.HERO);
});

lib.defineCommand("hero power target enemy hero", async () => {
    await lib.clickLocation(screenProfile.HERO_POWER);
    return lib.clickLocation(screenProfile.ENEMY_HERO);
});
