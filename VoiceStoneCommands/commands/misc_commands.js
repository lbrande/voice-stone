const lib = require("../lib.js");
const screenProfile = require("../screen_profile_2560_1440.js");

lib.numberRange(10).map((position, index) =>
    lib.defineCommand(`trade ${position}`, async gameData => {
        await lib.clickLocation(
            screenProfile.HAND[gameData.handSize - 1],
            index);
        return lib.clickLocation(screenProfile.TRADE);
    })
);

lib.numberRange(7).map((position, index) =>
    lib.defineCommand(`target minion ${position}`, async gameData => {
        await lib.clickLocation(
            screenProfile.BOARD,
            (6 - gameData.boardSize) / 2 + index);
        return lib.clickLocation(
            screenProfile.BOARD,
            (7 - gameData.boardSize) / 2 + index);
    })
);

lib.numberRange(7).map((position, index) =>
    lib.defineCommand(`target enemy ${position}`, async gameData => {
        return lib.clickLocation(
            screenProfile.ENEMY_BOARD,
            (7 - gameData.enemyBoardSize) / 2 + index);
    })
)

lib.defineCommand("target self", () => {
    return lib.clickLocation(screenProfile.HERO);
});

lib.defineCommand("target enemy hero", () => {
    return lib.clickLocation(screenProfile.ENEMY_HERO);
});

lib.defineCommand("cancel", () => {
    return lib.clickLocation(
        screenProfile.HIDE_SHOW_BUTTON,
        undefined,
        "right");
});

lib.defineCommand("end turn", () => {
    return lib.clickLocation(screenProfile.END_TURN_BUTTON);
});

lib.numberRange(10).map((position, index) =>
    lib.defineHoverCommand(`show ${position}`, gameData => {
        return lib.hoverLocation(
            screenProfile.HAND[gameData.handSize - 1],
            index);
    })
);

lib.numberRange(7).map((position, index) =>
    lib.defineHoverCommand(`show minion ${position}`, async gameData => {
        return lib.hoverLocation(
            screenProfile.BOARD,
            (7 - gameData.boardSize) / 2 + index);
    })
);

lib.numberRange(7).map((position, index) =>
    lib.defineHoverCommand(`show enemy ${position}`, async gameData => {
        return lib.hoverLocation(
            screenProfile.ENEMY_BOARD,
            (7 - gameData.enemyBoardSize) / 2 + index);
    })
)

lib.defineCommand("skip", () => {
    return lib.clickLocation(screenProfile.SKIP);
});
