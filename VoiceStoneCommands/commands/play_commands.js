const lib = require("../lib.js");
const screenProfile = require("../screen_profile_2560_1440.js");

lib.numberRange(10).map((position, index) =>
    lib.defineCommand(`play ${position}`, async gameData => {
        await lib.clickLocation(
            screenProfile.HAND[gameData.handSize - 1],
            index);
        return lib.clickLocation(screenProfile.BOARD, -1);
    })
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((boardPosition, boardIndex) =>
        lib.defineCommand(
            `play ${position} position ${boardPosition}`,
            async gameData => {
                await lib.clickLocation(
                    screenProfile.HAND[gameData.handSize - 1],
                    index);
                return lib.clickLocation(
                    screenProfile.BOARD,
                    (6 - gameData.boardSize) / 2 + boardIndex);
            })
    )
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((targetPosition, targetIndex) =>
        lib.defineCommand(
            `play ${position} target minion ${targetPosition}`,
            async gameData => {
                await lib.clickLocation(
                    screenProfile.HAND[gameData.handSize - 1],
                    index);
                await lib.clickLocation(screenProfile.BOARD, -1);
                await lib.clickLocation(
                    screenProfile.BOARD,
                    (7 - gameData.boardSize) / 2 + targetIndex);
                return lib.clickLocation(
                    screenProfile.BOARD,
                    (8 - gameData.boardSize) / 2 + targetIndex);
            })
    )
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((targetPosition, targetIndex) =>
        lib.defineCommand(
            `play ${position} target enemy ${targetPosition}`,
            async gameData => {
                await lib.clickLocation(
                    screenProfile.HAND[gameData.handSize - 1],
                    index);
                await lib.clickLocation(screenProfile.BOARD, -1);
                return lib.clickLocation(
                    screenProfile.ENEMY_BOARD,
                    (7 - gameData.enemyBoardSize) / 2 + targetIndex);
            })
    )
);

lib.numberRange(10).map((position, index) =>
    lib.defineCommand(`play ${position} target self`, async gameData => {
        await lib.clickLocation(
            screenProfile.HAND[gameData.handSize - 1],
            index);
        await lib.clickLocation(screenProfile.BOARD, -1);
        return lib.clickLocation(screenProfile.HERO);
    })
);

lib.numberRange(10).map((position, index) =>
    lib.defineCommand(`play ${position} target enemy hero`, async gameData => {
        await lib.clickLocation(
            screenProfile.HAND[gameData.handSize - 1],
            index);
        await lib.clickLocation(screenProfile.BOARD, -1);
        return lib.clickLocation(screenProfile.ENEMY_HERO);
    })
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((boardPosition, boardIndex) =>
        lib.numberRange(6).map((targetPosition, targetIndex) =>
            lib.defineCommand(
                `play ${position} position ${boardPosition} target minion ${targetPosition}`,
                async gameData => {
                    await lib.clickLocation(
                        screenProfile.HAND[gameData.handSize - 1],
                        index);
                    await lib.clickLocation(
                        screenProfile.BOARD,
                        (6 - gameData.boardSize) / 2 + boardIndex);
                    if (boardIndex > targetIndex) {
                        return lib.clickLocation(
                            screenProfile.BOARD,
                            (6 - gameData.boardSize) / 2 + targetIndex);
                    } else {
                        return lib.clickLocation(
                            screenProfile.BOARD,
                            (8 - gameData.boardSize) / 2 + targetIndex);
                    }
                })
        )
    )
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((boardPosition, boardIndex) =>
        lib.numberRange(7).map((targetPosition, targetIndex) =>
            lib.defineCommand(
                `play ${position} position ${boardPosition} target enemy ${targetPosition}`,
                async gameData => {
                    await lib.clickLocation(
                        screenProfile.HAND[gameData.handSize - 1],
                        index);
                    await lib.clickLocation(
                        screenProfile.BOARD,
                        (6 - gameData.boardSize) / 2 + boardIndex);
                    return lib.clickLocation(
                        screenProfile.ENEMY_BOARD,
                        (7 - gameData.enemyBoardSize) / 2 + targetIndex);
                })
        )
    )
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((boardPosition, boardIndex) =>
        lib.defineCommand(
            `play ${position} position ${boardPosition} target self`,
            async gameData => {
                await lib.clickLocation(
                    screenProfile.HAND[gameData.handSize - 1],
                    index);
                await lib.clickLocation(
                    screenProfile.BOARD,
                    (6 - gameData.boardSize) / 2 + boardIndex);
                return lib.clickLocation(screenProfile.HERO);
            })
    )
);

lib.numberRange(10).map((position, index) =>
    lib.numberRange(7).map((boardPosition, boardIndex) =>
        lib.defineCommand(
            `play ${position} position ${boardPosition} target enemy hero`,
            async gameData => {
                await lib.clickLocation(
                    screenProfile.HAND[gameData.handSize - 1],
                    index);
                await lib.clickLocation(
                    screenProfile.BOARD,
                    (6 - gameData.boardSize) / 2 + boardIndex);
                return lib.clickLocation(screenProfile.ENEMY_HERO);
            })
    )
);
