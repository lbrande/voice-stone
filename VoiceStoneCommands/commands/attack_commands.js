const lib = require("../lib.js");
const screenProfile = require("../screen_profile_2560_1440.js");

lib.numberRange(7).map((position, index) =>
    lib.numberRange(7).map((enemyPosition, enemyIndex) =>
        lib.defineCommand(
            `attack ${enemyPosition} with ${position}`,
            async gameData => {
                await lib.clickLocation(
                    screenProfile.BOARD,
                    (7 - gameData.boardSize) / 2 + index);
                return lib.clickLocation(
                    screenProfile.ENEMY_BOARD,
                    (7 - gameData.enemyBoardSize) / 2 + enemyIndex);
            })
    )
);

lib.numberRange(7).map((position, index) =>
    lib.defineCommand(`attack hero with ${position}`,
        async gameData => {
            await lib.clickLocation(
                screenProfile.BOARD,
                (7 - gameData.boardSize) / 2 + index);
            return lib.clickLocation(screenProfile.ENEMY_HERO);
        })
);

lib.defineCommand("attack hero with everything", async gameData => {
    for (let index = 0; index < gameData.boardSize; index++) {
        await lib.clickLocation(
            screenProfile.BOARD,
            (7 - gameData.boardSize) / 2 + index);
        await lib.clickLocation(screenProfile.ENEMY_HERO);
        await new Promise(resolve => setTimeout(resolve, 500));
    }
    await lib.clickLocation(screenProfile.HERO);
    return lib.clickLocation(screenProfile.ENEMY_HERO);
});

lib.numberRange(7).map((position, index) =>
    lib.defineCommand(
        `attack ${position} with hero`,
        async gameData => {
            await lib.clickLocation(screenProfile.HERO);
            return lib.clickLocation(
                screenProfile.ENEMY_BOARD,
                (7 - gameData.enemyBoardSize) / 2 + index);
        })
);  

lib.defineCommand("attack hero with hero", async () => {
    await lib.clickLocation(screenProfile.HERO);
    return lib.clickLocation(screenProfile.ENEMY_HERO);
});
