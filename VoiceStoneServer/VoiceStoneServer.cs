﻿using System;
using System.Net.Sockets;
using System.Net;

namespace VoiceStoneServer
{
    internal class VoiceStoneServer
    {
        private const byte READ = 0;
        private const byte WRITE = 1;

        static void Main(string[] args)
        {
            byte hand_size = 0;
            byte board_size = 0;
            byte enemy_board_size = 0;
            TcpListener server = new TcpListener(IPAddress.Parse("127.0.0.1"), 30310);
            server.Start();
            while (true)
            {
                using (var client = server.AcceptTcpClient())
                {
                    using (var stream = client.GetStream())
                    {
                        switch (stream.ReadByte())
                        {
                            case READ:
                                stream.WriteByte(hand_size);
                                stream.WriteByte(board_size);
                                stream.WriteByte(enemy_board_size);
                                break;
                            case WRITE:
                                hand_size = (byte)stream.ReadByte();
                                board_size = (byte)stream.ReadByte();
                                enemy_board_size = (byte)stream.ReadByte();
                                break;
                        }
                    }
                }
            }
        }
    }
}
