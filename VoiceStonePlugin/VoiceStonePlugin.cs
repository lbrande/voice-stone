﻿using System;
using System.Net.Sockets;
using System.Diagnostics;
using System.Windows.Controls;
using Hearthstone_Deck_Tracker.API;
using Hearthstone_Deck_Tracker.Plugins;

namespace VoiceStone
{
    public class VoiceStonePlugin : IPlugin
    {
        public string Name => "VoiceStone";
        public string Description => "A tool that enables you to play Hearthstone using only your voice.";
        public string ButtonText => "Restart server";
        public string Author => "Love Brandefelt";
        public Version Version => new Version(0, 0, 1);
        public MenuItem MenuItem => null;
        private const byte WRITE = 1;
        private Process _server = new Process();
        private byte _hand_size = 0;
        private byte _board_size = 0;
        private byte _enemy_board_size = 0;

        public void OnLoad()
        {
            _server.StartInfo.FileName = "VoiceStoneServer.exe";
            _server.StartInfo.WorkingDirectory = "C:\\Program Files\\VoiceStone";
            _server.Start();
        }

        public void OnUnload()
        {
            _server.Kill();
        }

        public void OnButtonPress()
        {
            _server.Kill();
            _server.Start();
        }

        public void OnUpdate()
        {
            if (_hand_size != Core.Game.PlayerHandCount || _board_size != Core.Game.PlayerBoardCount || _enemy_board_size != Core.Game.OpponentBoardCount)
            {
                _hand_size = (byte)Core.Game.PlayerHandCount;
                _board_size = (byte)Core.Game.PlayerBoardCount;
                _enemy_board_size = (byte)Core.Game.OpponentBoardCount;
                var client = new TcpClient("127.0.0.1", 30310);
                using (var stream = client.GetStream())
                {
                    stream.WriteByte(WRITE);
                    stream.WriteByte(_hand_size);
                    stream.WriteByte(_board_size);
                    stream.WriteByte(_enemy_board_size);
                }
            }
        }
    }
}
